<?php


namespace QYS\third\Pheanstalkd\Contract;

interface SocketFactoryInterface
{
    /**
     * This function must return a connected socket that is ready for reading / writing.
     * @return SocketInterface
     */
    public function create();
}
