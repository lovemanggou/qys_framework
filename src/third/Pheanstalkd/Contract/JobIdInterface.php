<?php


namespace QYS\third\Pheanstalkd\Contract;

interface JobIdInterface
{
    public function getId();
}
