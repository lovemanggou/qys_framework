<?php

namespace QYS\third\Pheanstalkd\Socket;

/**
 * A limited history of recent socket write length/success.
 *
 * Facilitates retrying zero-length writes a limited number of times,
 * avoiding infinite loops.
 *
 * Based on a patch from https://github.com/leprechaun
 * https://github.com/pda/pheanstalk/pull/24
 *
 * A bitfield could be used instead of an array for efficiency.
 *
 * @author  Paul Annesley
 * @package Pheanstalk
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class WriteHistory
{
    private $limit;
    private $data = [];

    public function __construct($limit)
    {
        $this->limit = $limit;
    }

    public function isFullWithNoWrites()
    {
        return $this->isFull() && !$this->hasWrites();
    }

    /**
     * Whether the history has reached its limit of entries.
     */
    public function isFull()
    {
        return count($this->data) >= $this->limit;
    }

    public function hasWrites()
    {
        return (bool)array_sum($this->data);
    }

    /**
     * Logs the return value from a write call.
     */
    public function log($write)
    {
        if ($this->isFull()) {
            array_shift($this->data);
        }

        $this->data[] = (int)$write;
    }
}
