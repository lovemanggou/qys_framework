<?php

namespace QYS\third\Pheanstalkd\Exception;

use QYS\third\Pheanstalkd\Exception;

/**
 * An exception originating from the beanstalkd client.
 */
class ClientException extends Exception
{
}
