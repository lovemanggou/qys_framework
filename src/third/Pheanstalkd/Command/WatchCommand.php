<?php

namespace QYS\third\Pheanstalkd\Command;

use QYS\third\Pheanstalkd\Contract\ResponseParserInterface;

/**
 * The 'watch' command.
 * Adds a tube to the watchlist to reserve jobs from.
 */
class WatchCommand extends TubeCommand implements ResponseParserInterface
{
    public function getCommandLine()
    {
        return 'watch ' . $this->tube;
    }

    public function parseResponse($responseLine, $responseData)
    {
        return $this->createResponse('WATCHING', [
            'count' => preg_replace('#^WATCHING (.+)$#', '$1', $responseLine),
        ]);
    }
}
