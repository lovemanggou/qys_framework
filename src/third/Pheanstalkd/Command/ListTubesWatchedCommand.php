<?php

namespace QYS\third\Pheanstalkd\Command;

use QYS\third\Pheanstalkd\YamlResponseParser;

/**
 * The 'list-tubes-watched' command.
 *
 * Lists the tubes on the watchlist.
 */
class ListTubesWatchedCommand extends AbstractCommand
{
    public function getCommandLine()
    {
        return 'list-tubes-watched';
    }

    public function getResponseParser()
    {
        return new YamlResponseParser(YamlResponseParser::MODE_LIST);
    }
}
