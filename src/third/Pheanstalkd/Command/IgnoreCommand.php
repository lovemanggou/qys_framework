<?php

namespace QYS\third\Pheanstalkd\Command;

use QYS\third\Pheanstalkd\Contract\ResponseInterface;
use QYS\third\Pheanstalkd\Contract\ResponseParserInterface;
use QYS\third\Pheanstalkd\Exception;

/**
 * The 'ignore' command.
 * Removes a tube from the watch list to reserve jobs from.
 */
class IgnoreCommand extends TubeCommand implements ResponseParserInterface
{
    public function getCommandLine()
    {
        return 'ignore ' . $this->tube;
    }

    public function parseResponse($responseLine, $responseData)
    {
        if (preg_match('#^WATCHING (\d+)$#', $responseLine, $matches)) {
            return $this->createResponse('WATCHING', [
                'count' => (int)$matches[1],
            ]);
        } elseif ($responseLine == ResponseInterface::RESPONSE_NOT_IGNORED) {
            throw new Exception\ServerException(
                $responseLine . ': cannot ignore last tube in watchlist'
            );
        } else {
            throw new Exception('Unhandled response: ' . $responseLine);
        }
    }
}
