<?php

namespace QYS\third\Pheanstalkd\Command;

use QYS\third\Pheanstalkd\Contract\ResponseInterface;
use QYS\third\Pheanstalkd\Contract\ResponseParserInterface;
use QYS\third\Pheanstalkd\Exception;

/**
 * The 'bury' command.
 * Puts a job into a 'buried' state, revived only by 'kick' command.
 */
class BuryCommand extends JobCommand implements ResponseParserInterface
{
    private $priority;

    public function __construct($job, $priority)
    {
        parent::__construct($job);
        $this->priority = $priority;
    }

    public function getCommandLine()
    {
        return sprintf(
            'bury %u %u',
            $this->jobId,
            $this->priority
        );
    }

    public function parseResponse($responseLine, $responseData)
    {
        if ($responseLine == ResponseInterface::RESPONSE_NOT_FOUND) {
            throw new Exception\ServerException(sprintf(
                '%s: Job %u is not reserved or does not exist.',
                $responseLine,
                $this->jobId
            ));
        } elseif ($responseLine == ResponseInterface::RESPONSE_BURIED) {
            return $this->createResponse(ResponseInterface::RESPONSE_BURIED);
        } else {
            throw new Exception('Unhandled response: ' . $responseLine);
        }
    }
}
