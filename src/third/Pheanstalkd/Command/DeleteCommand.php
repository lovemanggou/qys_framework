<?php

namespace QYS\third\Pheanstalkd\Command;

use QYS\third\Pheanstalkd\Contract\ResponseInterface;
use QYS\third\Pheanstalkd\Contract\ResponseParserInterface;
use QYS\third\Pheanstalkd\Exception;

/**
 * The 'delete' command.
 * Permanently deletes an already-reserved job.
 */
class DeleteCommand extends JobCommand implements ResponseParserInterface
{
    public function getCommandLine()
    {
        return 'delete ' . $this->jobId;
    }

    public function parseResponse($responseLine, $responseData)
    {
        if ($responseLine == ResponseInterface::RESPONSE_NOT_FOUND) {
            throw new Exception\ServerException(sprintf(
                'Cannot delete job %u: %s',
                $this->jobId,
                $responseLine
            ));
        }

        return $this->createResponse($responseLine);
    }
}
