<?php

namespace QYS\third\Pheanstalkd\Command;

use QYS\third\Pheanstalkd\YamlResponseParser;

/**
 * The 'list-tubes' command.
 *
 * List all existing tubes.
 */
class ListTubesCommand extends AbstractCommand
{
    public function getCommandLine()
    {
        return 'list-tubes';
    }

    public function getResponseParser()
    {
        return new YamlResponseParser(YamlResponseParser::MODE_LIST);
    }
}
