<?php

namespace QYS\third\Pheanstalkd\Command;

use QYS\third\Pheanstalkd\YamlResponseParser;

/**
 * The 'stats-tube' command.
 * Gives statistical information about the specified tube if it exists.
 */
class StatsTubeCommand extends TubeCommand
{
    public function getCommandLine()
    {
        return sprintf('stats-tube %s', $this->tube);
    }

    public function getResponseParser()
    {
        return new YamlResponseParser(
            YamlResponseParser::MODE_DICT
        );
    }
}
