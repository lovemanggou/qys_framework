<?php

namespace QYS\third\Pheanstalkd\Command;

use QYS\third\Pheanstalkd\Contract\ResponseParserInterface;

/**
 * The 'list-tube-used' command.
 *
 * Returns the tube currently being used by the client.
 */
class ListTubeUsedCommand extends AbstractCommand implements ResponseParserInterface
{
    public function getCommandLine()
    {
        return 'list-tube-used';
    }

    public function parseResponse($responseLine, $responseData)
    {
        return $this->createResponse('USING', [
            'tube' => preg_replace('#^USING (.+)$#', '$1', $responseLine),
        ]);
    }
}
