<?php

namespace QYS\third\Pheanstalkd\Command;

use QYS\third\Pheanstalkd\YamlResponseParser;

/**
 * The 'stats' command.
 *
 * Statistical information about the system as a whole.
 */
class StatsCommand extends AbstractCommand
{
    public function getCommandLine()
    {
        return 'stats';
    }

    public function getResponseParser()
    {
        return new YamlResponseParser(
            YamlResponseParser::MODE_DICT
        );
    }
}
