<?php

namespace QYS\Proxy;

class HttpProxyServer
{
    static $frontendCloseCount = 0;
    static $backendCloseCount = 0;
    static $frontends = array();
    static $backends = array();
    static $serv;


    static function getClient($fd)
    {
        if (!isset(HttpProxyServer::$frontends[$fd])) {
            $client = new \swoole_http_client('127.0.0.1', 10000);
            $client->set(array('keep_alive' => 0));
            HttpProxyServer::$frontends[$fd] = $client;
            $client->on('connect', function ($cli) use ($fd) {
                HttpProxyServer::$backends[$cli->sock] = $fd;
            });
            $client->on('close', function ($cli) use ($fd) {
                self::$backendCloseCount++;
                unset(HttpProxyServer::$backends[$cli->sock]);
                unset(HttpProxyServer::$frontends[$fd]);
//                echo self::$backendCloseCount . "\tbackend[{$cli->sock}]#[{$fd}] close\n";
            });
        }
        return HttpProxyServer::$frontends[$fd];
    }
}