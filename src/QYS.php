<?php

namespace QYS;

use QYS\Core\Config;
use QYS\Protocol\Response;
use QYS\Server\Factory;
use QYS\Util\Formater;
use QYS\Util\Log;
use function date_default_timezone_set;
use function set_error_handler;

class QYS
{
    const VERSION = '0.1';
    const IS_DEV = true;
    private static $projPath;
    private static $appPath = 'apps';
    private static $frame_work_path = "";
    private static $configPath;

    public static function getFrameWorkPath(): string
    {
        return self::$frame_work_path;
    }

    public static function getVersion(): string
    {
        return self::VERSION;
    }

    public static function isDev(): bool
    {
        return self::IS_DEV;
    }

    public static function bye()
    {
        throw new DummyExitError("bye");
    }

    public static function exit()
    {
        throw new DummyExitError("exit");
    }


    final public static function exceptionHandler($exception)
    {
        $ret = Formater::exception($exception);
        Log::info('exception', $ret);
//        return Response::display($ret);
    }

    final public static function fatalHandler()
    {
        $error = \error_get_last();
        if (empty($error)) {
            return "";
        }
        if (!in_array($error['type'], array(E_ERROR, E_PARSE, E_CORE_ERROR, E_COMPILE_ERROR))) {
            return "";
        }
        Response::status('200');
        $ret = Formater::fatal($error);
        Log::info('fatal', $ret);
        return Response::display($ret);
    }

    public static function runFile($projPath, $configPath, $filePath)
    {
        self::$frame_work_path = dirname(__FILE__);
        if (!defined('DS')) {
            define('DS', DIRECTORY_SEPARATOR);
        }
        self::setProjPath($projPath);


        if (!\file_exists($projPath . DS . $configPath)) {
            throw new \Exception("配置文件不存在" . $projPath . DS . $configPath);
        }

        self::setConfigPath($configPath);

        Config::load(self::getConfigPath());

        $gameName = Config::get('game_name');
        $appName = Config::get('app_name');
        $version = Config::get('version');
        $GLOBALS['GAME_NAME'] = $gameName;
        $GLOBALS['APP_NAME'] = $appName;
        $GLOBALS['VERSION'] = $version;

        require_once($filePath);
    }

    public static function getConfigPath(): string
    {
        return self::getProjPath() . DS . self::$configPath;
    }

    public static function setConfigPath($path)
    {
        self::$configPath = $path;
    }

    public static function getProjPath()
    {
        return self::$projPath;
    }

    public static function setProjPath($projPath)
    {
        self::$projPath = $projPath;
    }

    public static function run($projPath, $configPath = null)
    {

        self::$frame_work_path = dirname(__FILE__);
        if (!defined('DS')) {
            define('DS', DIRECTORY_SEPARATOR);
        }
        self::setProjPath($projPath);


        if (!\file_exists($projPath . DS . $configPath)) {
            throw new \Exception("配置文件不存在" . $projPath . DS . $configPath);
        }

        self::setConfigPath($configPath);
        Config::load(self::getConfigPath());

        $serverMode = Config::get('server_mode', 'Http');

        if (Config::getField('project', 'error_handler')) {
            set_error_handler(Config::getField('project', 'error_handler'));
        }

        $timeZone = Config::get('time_zone', 'Asia/Shanghai');
        date_default_timezone_set($timeZone);

        $service = Factory::getInstance($serverMode);
        $service->run();
    }
}
