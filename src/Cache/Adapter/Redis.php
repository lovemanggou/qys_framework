<?php
/**
 * Created by PhpStorm.
 * User: xhkj
 * Date: 2018/7/18
 * Time: 下午1:00
 */

namespace QYS\Cache\Adapter;

use QYS\Cache\ICache;
use QYS\Db\Redis as RD;

class Redis implements ICache
{
    private $redis;

    public function __construct($config)
    {
        if (empty($this->redis)) {
            $this->redis = RD::getInstance($config);
        }
    }

    public function enable()
    {
        return true;
    }

    public function selectDb($db)
    {
        $this->redis->select($db);
    }

    public function add($key, $value, $expiration = 300)
    {
        if ($this->redis->setnx($key, $value)) {
            $this->redis->expire($key, $expiration);
            return true;
        } else {
            return false;
        }
        return false;
    }

    public function addToCache($key, $value, $expiration = 300)
    {
        return $this->set($key, $value, $expiration);
    }

    public function set($key, $value, $expiration = 300)
    {
        return $this->redis->setex($key, $expiration, $value);
    }

    public function getCache($key)
    {
        return $this->get($key);
    }

    public function get($key)
    {
        return $this->redis->get($key);
    }

    public function delete($key)
    {
        return $this->redis->delete($key);
    }

    public function increment($key, $offset = 1)
    {
        return $this->redis->incrBy($key, $offset);
    }

    public function decrement($key, $offset = 1)
    {
        return $this->redis->decBy($key, $offset);
    }

    public function clear()
    {
        return $this->redis->flushDB();
    }
}