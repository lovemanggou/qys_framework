<?php

namespace QYS\Cache;

use QYS\Core\Config as QYSConfig;
use QYS\Core\Factory as QYSFactory;

class Factory
{
    private static $_map = [
        'Redis' => 1,
    ];

    /**
     * @param string $adapter
     * @param null $config
     * @return ICache
     * @throws \Exception
     */
    public static function getInstance($adapter = 'Redis', $config = null)
    {
        if (empty($config)) {
            $config = QYSConfig::get('cache');
            if (!empty($config['adapter'])) {
                $adapter = $config['adapter'];
            }
        }
        $className = __NAMESPACE__ . "\\Adapter\\{$adapter}";
        return QYSFactory::getInstance($className, $config);
    }
}
