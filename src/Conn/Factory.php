<?php
/**
 * Created by PhpStorm.
 * User: xhkj
 * Date: 2018/4/25
 * Time: 下午7:17
 */

namespace QYS\Conn;

use QYS\Core\Config as QYSConfig;
use QYS\Core\Factory as CFactory;


class Factory
{
    public static function getInstance($adapter = "Swoole", $config = null)
    {
        if (empty($config)) {
            $config = QYSConfig::get('connection');
            if (!empty($config['adapter'])) {
                $adapter = $config['adapter'];
            }
        }

        $className = __NAMESPACE__ . "\\Adapter\\{$adapter}";
        return CFactory::getInstance($className, $config);
    }

}
