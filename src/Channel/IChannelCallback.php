<?php
/**
 * Created by PhpStorm.
 * User: xhkj
 * Date: 2018/4/27
 * Time: 下午1:43
 */

namespace QYS\Channel;


interface IChannelCallback
{
    public function onChannelConnect($cli);

    public function onChannelClose($cli);

    public function onChannelError($cli);

    public function onChannelReceive($cli, $data);

}