<?php
/**
 * Created by PhpStorm.
 * User: xhkj
 * Date: 2018/4/27
 * Time: 下午1:31
 */

namespace QYS\Channel;

class ChannelClient
{
    private $callback = null;
    private $client = null;
    private $sid = null;
    private $wid = null;
    private $heartbeat_check_interval = null;

    public function __construct($server, $heartbeat_check_interval)
    {
        if (empty($server)) {
            throw new \Exception("传入的类型不是一个swoole的服务");
        }
        $sid = getenv('SERVER_ID');
        $wid = $server->worker_id;
        if (!isset($wid)) {
            throw new \Exception("传入的类型不是一个swoole的服务");
        }
        $this->sid = $sid;
        $this->wid = $wid;
        $this->heartbeat_check_interval = $heartbeat_check_interval;
    }

    public function setCallback($callback)
    {
        $this->callback = $callback;
    }


    public function send($data)
    {
        $this->innersend('client', 'sendmessage', $data);
    }

    private function innersend($c, $m, $data)
    {
        $package = array('sid' => $this->sid, 'wid' => $this->wid, 'c' => $c, 'm' => $m, 'data' => $data);
        $sendStr = serialize($package);
        $sendData = pack('N', strlen($sendStr)) . $sendStr;
        $this->client->send($sendData);
    }


    public function connect($host, $port)
    {
        if ($this->isConnected()) {
            throw new \Exception('通道接口没有关闭');
        }

        $client = new \swoole_client(SWOOLE_SOCK_TCP, SWOOLE_SOCK_ASYNC);
        $client->set(array(
            'open_length_check' => 1,
            'package_length_type' => 'N',
            'package_length_offset' => 0,       //第N个字节是包长度的值
            'package_body_offset' => 4,       //第几个字节开始计算长度
            'package_max_length' => 2000000,  //协议最大长度
        ));

        $this->client = $client;

        $self = $this;
        $client->on("receive", function ($cli, $data) use ($self) {
            $package = unserialize(substr($data, 4));
            $self->callback->onChannelReceive($self, $package['data']);
        });

        $client->on("connect", function ($cli) use ($self) {
            $self->innersend('core', 'login', array());
            $self->callback->onChannelConnect($self);
        });
        $client->on("error", function ($cli) use ($self) {
            $self->callback->onChannelError($self);
        });
        $client->on("close", function ($cli) use ($self) {
            $self->callback->onChannelClose($self);
        });
        $this->client->connect($host, $port, 0.5);

        swoole_timer_tick($this->heartbeat_check_interval * 1000, function ($timer_id) use ($self) {
            if ($self->isConnected())
                $self->innersend('core', 'hearbeat', array());
            else
                swoole_timer_clear($timer_id);
        });
    }

    public function isConnected()
    {
        if (empty($this->client)) {
            return false;
        }

        return $this->client->isConnected();
    }

    public function close()
    {
        if (!empty($this->client) && $this->isConnected()) {
            $this->client->close();
            $this->client = null;
        }
    }
}