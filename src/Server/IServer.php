<?php

namespace QYS\Server;


interface IServer
{
    public function run();
}
