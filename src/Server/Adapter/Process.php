<?php

namespace QYS\Server\Adapter;

use QYS\Core\Config;
use QYS\Core\Factory as CFactory;
use QYS\Server\IServer;
use QYS\Socket\Factory as SFactory;

class Process implements IServer
{
    public function run()
    {
        $config = Config::get('process');
        if (empty($config)) {
            throw new \Exception("process config empty");
        }

        $process = SFactory::getInstance('Process', $config);
        if (method_exists($process, 'setCallBack')) {
            $client = CFactory::getInstance($config['callback_class']);
            // $process->setCallBack($client);
        }

        $process->run();
    }
}