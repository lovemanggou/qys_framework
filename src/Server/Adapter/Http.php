<?php

namespace QYS\Server\Adapter;

use QYS\Core;
use QYS\Server\IServer;

class Http implements IServer
{

    public function run()
    {
        Protocol\Request::setServer(
            Protocol\Factory::getInstance(
                Core\Config::getField('Project', 'protocol', 'Http')
            )
        );
        Protocol\Request::parse($_REQUEST);
        return Core\Route::route();
    }

}