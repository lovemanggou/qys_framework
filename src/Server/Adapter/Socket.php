<?php

namespace QYS\Server\Adapter;

use QYS\Core\Config;
use QYS\Core\Factory as CFactory;
use QYS\Protocol\Factory as ZProtocol;
use QYS\Protocol\Request;
use QYS\Server\IServer;
use QYS\Socket\Adapter\Swoole;
use QYS\Socket\Factory as SFactory;

class Socket implements IServer
{
    public function run()
    {
        $config = Config::get('socket');
        if (empty($config)) {
            throw new \Exception("socket config empty");
        }

        /** @var Swoole $socket */
        $socket = SFactory::getInstance('Swoole', $config);

        if (method_exists($socket, 'setCallBack')) {
            $client = CFactory::getInstance($config['callback_class']);
            $socket->setCallBack($client);
        }

        Request::setServer(ZProtocol::getInstance(Config::getField('socket', 'protocol')));
        Request::setLongServer();
        Request::setHttpServer(0);

        $socket->run();
    }
}