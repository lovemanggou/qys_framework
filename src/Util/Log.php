<?php

namespace QYS\Util;

use QYS\Core\Config;
use QYS\QYS;

class Log
{
    const SEPARATOR = "\t";

    private static $xhprof = false;
    private static $records;

    private static $DEBUG_TRACE = false;

    public static function getMicroTime()
    {
        list($usec, $sec) = \explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }


    public static function info($type, $params = array())
    {
        $t = \date("Ymd");
        $logPath = Config::getField('project', 'log_path', '');
        if (empty($logPath)) {
            $dir = QYS::getProjPath() . DS . 'log' . DS . $t;
        } else {
            $dir = $logPath . DS . $t;
        }
        Dir::make($dir);
        $str = \date('Y-m-d H:i:s', Config::get('now_time', time())) . self::SEPARATOR . \implode(self::SEPARATOR, array_map('QYS\Util\Log::myJson', $params));
        $logFile = $dir . \DS . $type . '.log';
        \file_put_contents($logFile, $str . "\n", FILE_APPEND | LOCK_EX);
    }

    public static function myJson($data)
    {
        return json_encode($data, JSON_UNESCAPED_UNICODE);
    }
}
