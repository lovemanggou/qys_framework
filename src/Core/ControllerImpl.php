<?php

namespace QYS\Core;

use QYS\HttpException;
use QYS\Protocol\Request;
use QYS\Protocol\Response;
use ReflectionException;
use ReflectionMethod;

abstract class ControllerImpl
{
    private ?Response $response = null;
    private ?Request $request = null;
    private string $handelFunc = '';

    public function getHandelFunc(): string
    {
        return $this->handelFunc;
    }

    public function setHandelFunc(string $handelFunc): self
    {
        $this->handelFunc = $handelFunc;
        return $this;
    }

    public function setData(string $key, $data)
    {
        $this->$key = $data;
    }

    /**
     * @throws HttpException
     */
    public function getData(string $key, bool $throwErr = false)
    {
        if (!isset($this->$key) && $throwErr === true) {
            throw new HttpException('illegal key');
        }
        return $this->$key;
    }

    public function init()
    {
    }

    public function invoke()
    {
        return call_user_func([$this, $this->handelFunc]);
    }

    /**
     * 前置事件
     * @throws ReflectionException
     */
    public function getBeforeActionFunc(): array
    {
        $arr = [];
        $refClass = new ReflectionMethod($this, $this->handelFunc);
        $events = $refClass->getAttributes(Event::class);
        foreach ($events as $event) {
            $event = $event->newInstance();
            /** @var Event $class */
            /** @var Event $event */
            $class = new $event->className($event->name, $event->className);
            var_dump($event->className);
            if (method_exists($class, $class->beforeFunc)) {
                $arr[$class->name] = [
                    'class' => $class,
                    'method' => $class->beforeFunc,
                ];
            }
        }
        return $arr;
    }

    /**
     * 后置事件
     * @throws ReflectionException
     */
    public function getAfterActionFunc(): array
    {
        $arr = [];
        $refClass = new ReflectionMethod($this, $this->handelFunc);
        $events = $refClass->getAttributes(Event::class);
        foreach ($events as $event) {
            $event = $event->newInstance();
            /** @var Event $class */
            /** @var Event $event */
            $class = new $event->className($event->name, $event->className);
            if (method_exists($class, $class->afterFunc)) {
                $arr[$class->name] = [
                    'class' => $class,
                    'method' => $class->afterFunc,
                ];
            }
        }
        return $arr;
    }

    /**
     * 异常退出事件
     * @return array
     * @throws ReflectionException
     */
    public function getExitActionFunc(): array
    {
        $arr = [];
        $refClass = new ReflectionMethod($this, $this->handelFunc);
        $events = $refClass->getAttributes(Event::class);
        foreach ($events as $event) {
            $event = $event->newInstance();
            /** @var Event $class */
            /** @var Event $event */
            $class = new $event->className($event->name, $event->className);
            if (method_exists($class, $class->exitFunc)) {
                $arr[$class->name] = [
                    'class' => $class,
                    'method' => $class->exitFunc,
                ];
            }
        }
        return $arr;
    }

    public function send(array $data = []): void
    {
        $data = json_encode($data);
        $this->getResponse()->say($data);
    }

    public function setRequest(Request $request): self
    {
        $this->request = $request;
        return $this;
    }

    public function getRequest(): Request
    {
        return $this->request;
    }

    public function setResponse(Response $response): self
    {
        $this->response = $response;
        return $this;
    }

    public function getResponse(): Response
    {
        return $this->response;
    }

}
