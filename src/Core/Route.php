<?php

namespace QYS\Core;

use Exception;
use QYS\HttpException;
use QYS\Log\Log;
use QYS\Protocol\Request;
use QYS\QYS;
use QYS\Session\Swoole as SESSION;
use QYS\Waf\Waf;


function isLocalIPAddress($IPAddress): bool
{
    if ($IPAddress == '127.0.0.1') {
        return true;
    }
    return (!filter_var($IPAddress, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE));
}

class Route
{
    /**
     * @param $request
     * @param $response
     * @return void
     * @throws Exception
     * @author yin
     */
    public static function route($request, $response): void
    {
        $server = Config::get('server', []);

        $path_info = $request->getPathInfo();

        if (self::endWith($path_info, "/")) {
            $path_info = substr($path_info, 0, -1);
        }

        Waf::access($request, $response);
        if (!array_key_exists($path_info, $server)) {
            $response->status(404);
        }
        $route = $server[$path_info];
        if (!isset($route['content_by_php_class'])) {
            $response->status(405);
            $response->say('undefined content_by_php_class');
            return;
        }

        $classname = strval($route['content_by_php_class']['class']);
        $func = strval($route['content_by_php_class']['method']);
        if (!$classname || !$func) {
            $response->status(405);
            $response->say('unkonwn class or method.');
            return;
        }

        $controller = new $classname();
        if (!($controller instanceof ControllerImpl)) {
            $response->status(405);
            $response->say('class type not allow.');
            return;
        }

        $controller->init();
        $controller->setHandelFunc($func);
        $controller->setRequest($request);
        $controller->setResponse($response);

        if (isset($route['content_by_php_class']['params'])) {
            $params = $route['content_by_php_class']['params'];
            foreach ($params as $k => $v) {
                $controller->setData($k, $v);
            }
        }

        $handEventFunc = function ($event, $res) use ($controller) {
            call_user_func([$event['class'], $event['method']], $controller, $res);
        };

        $list = $controller->getBeforeActionFunc();
        if ($list) {
            foreach ($list as $v) {
                $handEventFunc($v, []);
            }
        }
        try {
            $res = $controller->invoke();
        } catch (HttpException|Exception $th) {
            $list = $controller->getExitActionFunc();
            if ($list) {
                foreach ($list as $v) {
                    $handEventFunc($v, []);
                }
            }
            if ($th instanceof HttpException) {
                $controller->send(['errcode' => $th->getCode(), 'errmsg' => $th->getMessage(), 'data' => $th->getData()]);
            } else {
                $controller->send(['errcode' => $th->getCode(), 'errmsg' => $th->getMessage()]);
                throw $th;
            }
            return;
        }
        $list = $controller->getAfterActionFunc();
        if ($list) {
            foreach ($list as $v) {
                $handEventFunc($v, $res);
            }
        }
        $controller->send($res);

        if (Request::isLongServer()) {
            SESSION::save();
        }
        Log::access($request, $response);
    }

    private static function endWith($haystack, $needle): bool
    {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }
        return (substr($haystack, -$length) === $needle);
    }
}
