<?php

namespace QYS\Core;

// use QYS\Util\Dir;

class Config
{
    private static $config;
    private static $configPath;

    public static function load($configPath)
    {
        // $files = Dir::tree($configPath, "/.php$/");
        $config = array();
        if (function_exists("opcache_invalidate")) {
            \opcache_invalidate($configPath);
        }
        $config = include "{$configPath}";
        self::$config = $config;
        self::$configPath = $configPath;
        return self::$config;
    }

    public static function get($key, $default = null, $throw = false)
    {
        self::checkTime();
        $result = isset(self::$config[$key]) ? self::$config[$key] : $default;
        if ($throw && is_null($result)) {
            throw new \Exception("{key} config empty");
        }
        return $result;
    }

    public static function checkTime()
    {
        // if (true) {
        //     if (self::$nextCheckTime < time() && !empty(self::$reloadPath)) {
        //         foreach (self::$reloadPath as $path) {
        //             if (!is_dir($path)) {
        //                 continue;
        //             }
        //             \clearstatcache($path);
        //             if (self::$lastModifyTime[$path] < \filectime($path)) {
        //                 self::mergePath($path);
        //             }
        //         }
        //     }
        // }
        // return;
    }

    public static function set($key, $value, $set = true)
    {
        if ($set) {
            self::$config[$key] = $value;
        } else {
            if (empty(self::$config[$key])) {
                self::$config[$key] = $value;
            }
        }

        return true;
    }

    public static function getField($key, $field, $default = null, $throw = false)
    {
        self::checkTime();
        $result = isset(self::$config[$key][$field]) ? self::$config[$key][$field] : $default;
        if ($throw && is_null($result)) {
            throw new \Exception("{key} config empty");
        }
        return $result;
    }

    public static function setField($key, $field, $value, $set = true)
    {
        if ($set) {
            self::$config[$key][$field] = $value;
        } else {
            if (empty(self::$config[$key][$field])) {
                self::$config[$key][$field] = $value;
            }
        }

        return true;
    }

    public static function all()
    {
        return self::$config;
    }

}
