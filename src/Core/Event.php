<?php

namespace QYS\Core;

use Attribute;

#[Attribute(Attribute::TARGET_METHOD | Attribute::IS_REPEATABLE)]
class Event
{
    public string $name;
    public string $className;
    public string $beforeFunc;
    public string $afterFunc;
    public string $exitFunc;

    public function __construct(string $name = '', string $className = '', string $beforeFunc = '', string $afterFunc = '', string $exitFunc = '')
    {
        $this->name = $name;
        $this->className = $className;
        $this->beforeFunc = $beforeFunc ?: 'beforeHandel';
        $this->afterFunc = $afterFunc ?: 'afterHandel';
        $this->exitFunc = $exitFunc ?: 'exitHandel';
    }
}
