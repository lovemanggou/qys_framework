<?php

namespace QYS\Protocol;

use QYS\Core\Config;
use QYS\View\Factory as ZView;


class Response
{
    const RESPONSE_TIME_KEY = 'X-Run-Time';
    /**
     * @var \Swoole\Http\Response
     */
    private $_response = null;
    private $_request = null;
    private $_headers = array();
    private $_response_time = null;
    private $_execute_time = 0;
    private $chunk = null;

    public function setRequest($request)
    {
        $this->_request = $request;
    }

    function __destruct()
    {
//        $req_id = $this->_request->getRequestId();
//        print "Request Destroying $req_id".PHP_EOL;
//        Debug::log(array("file"=>__FILE__,"req_id"=>$req_id));
        if (isset($this->chunk) && $this->chunk !== '') {
            $this->getResponse()->end($this->chunk);
        }
    }

    public function getResponse(): ?\Swoole\Http\Response
    {
        return $this->_response;
    }

    public function setResponse($response)
    {
        $this->_response = $response;
        if ($response) {
            $this->_headers = array();
            $this->_response_time = 0;
            $this->_execute_time = 0;
        }
    }

    public function say($data)
    {
        if (empty($this->chunk)) {
            $this->chunk = "";
        }
        $this->chunk .= "$data";
        $this->getResponse()->end($this->chunk);
        $this->chunk = '';
    }


    /**
     * @param $model
     * @return mixed
     * @throws \Exception
     * @desc reponse数据格式输出
     */
    public function display($model)
    {
        $this->_response_time = microtime(true);
        $key = Config::getField('project', 'response_time_key', self::RESPONSE_TIME_KEY);
        $startTime = $this->getRequestTime(true);
        $this->_execute_time = $this->_response_time - $startTime;
        $this->addHeader($key . '-Start', $startTime);
        $this->addHeader($key . '-End', self::$_response_time);
        $this->addHeader($key, self::$_execute_time);
        return $this->getContent($model);
    }

    /**
     * @param $key
     * @param $val
     * @desc 添加一个response头
     */
    public function addHeader($key, $val)
    {
        $this->_headers[$key] = $val;
    }

    /**
     * @param $model
     * @return mixed
     * @throws \Exception
     */
    public function getContent($model)
    {
        if (null === $model || false === $model) {
            return $model;
        }
        if (is_array($model) && !empty($model['_view_mode'])) {
            $viewMode = $model['_view_mode'];
            unset($model['_view_mode']);
        } else {
            $viewMode = $this->getViewMode();
            if (empty($viewMode)) {
                $viewMode = Config::getField('project', 'view_mode', '');
                if (empty($viewMode)) {
                    if ($this->_request->isAjax() || Request::isLongServer()) {
                        $viewMode = 'Json';
                    } else {
                        $viewMode = 'Php';
                    }
                }
            }
        }

        $view = ZView::getInstance($viewMode);
        if ('Php' === $viewMode) {
            $_tpl_file = Request::getTplFile();
            if (is_array($model) && !empty($model['_tpl_file'])) {
                $_tpl_file = $model['_tpl_file'];
                unset($model['_tpl_file']);
            }

            if (empty($_tpl_file)) {
                throw new \Exception("tpl file empty");
            }
            $view->setTpl($_tpl_file);
        }
        $view->setModel($model);
        return $view->display();
    }

    /**
     * @return array
     * @desc 获取所有response待发响应头
     */
    public function getHeaders(): array
    {
        return $this->_headers;
    }

    public function getChunk()
    {
        return $this->chunk;
    }

    public function setChunk($chunk)
    {
        $this->chunk = $chunk;
    }

    /**
     * @desc 发送所有http response header头
     */
    public function sendHttpHeader()
    {
        if (!empty($this->_headers) && Request::isHttp()) {
            foreach ($this->_headers as $key => $val) {
                $this->header($key, $val);
            }
        }
    }

    /**
     * @param $key
     * @param $val
     * @desc 发送http头
     */
    public function header($key, $val)
    {
        if ($this->_response) {
            $this->_response->header($key, $val);
            return;
        }

        \header("{$key}: {$val}");
    }

    /**
     * @param $code
     * @desc 设置响应状态码
     */
    public function status($code)
    {
        if ($this->_response) {
            $this->_response->status($code);
            return;
        }

        \http_response_code($code);

    }

    /**
     * @param $key
     * @param string $value
     * @param int $expire
     * @param string $path
     * @param string $domain
     * @param bool $secure
     * @param bool $httponly
     * @desc 设置cookie
     */
    public function setCookie($key, string $value = '', int $expire = 0, string $path = '/', string $domain = '', bool $secure = false, bool $httponly = false)
    {
        if ($this->_response) {
            $this->_response->cookie($key, $value, $expire, $path, $domain, $secure, $httponly);
            return;
        }
        \setcookie($key, $value, $expire, $path, $domain, $secure, $httponly);

    }

    /**
     * @param $key
     * @param string $value
     * @param int $expire
     * @param string $path
     * @param string $domain
     * @param bool $secure
     * @param bool $httponly
     * @desc 设置原始cookie
     */
    public function setRawCookie($key, string $value = '', int $expire = 0, string $path = '/', string $domain = '', bool $secure = false, $httponly = false)
    {
        if ($this->_response) {
            $this->_response->rawcookie($key, $value, $expire, $path, $domain, $secure, $httponly);
        }
        \setrawcookie($key, $value, $expire, $path, $domain, $secure, $httponly);
    }

    /**
     * @return null
     * @desc 返回response响应的时间戳
     */
    public function getResponseTime()
    {
        return $this->_response_time;
    }

    /**
     * @return int
     * @desc 返回一次请求的执行时间
     */
    public function getExecuteTime(): int
    {
        return $this->_execute_time;
    }

}
