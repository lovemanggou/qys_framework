<?php

namespace QYS\Protocol\Adapter;

use QYS\Core\Config;
use QYS\Protocol\IProtocol;
use QYS\Protocol\Request;

class Http implements IProtocol
{
    public function parse($data)
    {
        Request::init($data, Config::getField('project', 'view_mode', 'Php'));
        return true;
    }
}
