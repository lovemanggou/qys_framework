<?php

namespace QYS\Protocol;

use QYS\Core\Factory as CFactory;

class Factory
{
    private static $_map = [
        'Cli' => 1,
        'Http' => 1,
        'Json' => 1,
    ];

    public static function getInstance($adapter = 'Http')
    {
        $adapter = ucfirst(strtolower($adapter));
        if (isset(self::$_map[$adapter])) {
            $className = __NAMESPACE__ . "\\Adapter\\{$adapter}";
        } else {
            $className = $adapter;
        }
        return CFactory::getInstance($className);
    }
}