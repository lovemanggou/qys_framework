<?php

namespace QYS\Protocol;

use QYS\Core\Config as QYSConfig;

class Request
{

    const REQUEST_ID_KEY = 'X-Request-Id';
    const REQUEST_TIME_KEY = 'X-Request-Time';
    private static $_long_server = 0;
    private static $_is_http = 1;
    /**
     * @var IProtocol
     */
    private static $_server;
    private static $_serv;
    private $_params;
    private $_view_mode = null;
    private $_fd = null;
    /** @var \Swoole\Http\Request */
    private $_request = null;
    private $_socket = null;
    private $_headers = array();
    /*
     * swoole服务器进程
     */
    private $_request_time = 0;

    public static function setSocketServer($serv)
    {
        self::$_serv = $serv;
    }

    public static function getSocketServer()
    {
        return self::$_serv;
    }

    /**
     * @return IProtocol
     * @desc 获取protocal对像
     */
    public static function getServer()
    {
        return self::$_server;
    }

    /**
     * @param $server
     * @desc 设置protocal对像
     */
    public static function setServer($server)
    {
        self::$_server = $server;
    }

    /**
     * @param $data
     * @return mixed
     * @desc 解析请求体
     */
    public static function parse($data)
    {
        return self::$_server->parse($data);
    }

    /**
     * @param int $tag
     * @desc 设置为长驻服务(swoole模式)
     */
    public static function setLongServer($tag = 1)
    {
        self::$_long_server = $tag;
    }

    /**
     * @param int $tag
     * @desc 是否swoole_http运行 (swoole模式)
     */
    public static function setHttpServer($tag = 1)
    {
        self::$_is_http = $tag;
    }

    /**
     * @param int $timeOut
     * @return bool
     * @desc 检测请求是否已超时
     */
    public static function checkRequestTimeOut($timeOut = 0)
    {
        $key = QYSConfig::getField('project', 'request_time_key', self::REQUEST_TIME_KEY);
        if (!isset(self::$_headers[$key])) {
            return false;
        }

        if (!$timeOut) {
            if (!empty(self::$_headers['X-Request-Timeout'])) {
                $timeOut = self::$_headers['X-Request-Timeout'];
            }
        }

        if ($timeOut <= 0) {
            return false;
        }

        $startTime = self::$_headers[$key];
        $nowTime = microtime(true);
        return $nowTime - $startTime > $timeOut;
    }

    function __destruct()
    {

    }

    /**
     * @param $ctrl
     * @param $method
     * @param array $params
     * @param null $viewMode
     * @throws \Exception
     * @desc 请求初始化
     */
    public function init(array $params, $viewMode = null)
    {
        $this->_params = $params;
        $this->_view_mode = $viewMode;
        $this->setRequestId();
    }

    /**
     * @param null $requestId
     * @return mixed|null|string
     * @desc 设置请求唯一id
     */
    public function setRequestId($requestId = null)
    {
        if (empty($requestId)) {
            $requestId = $this->getRequestId(true);
        }
        $requestIdKey = QYSConfig::getField('project', 'request_id_key', self::REQUEST_ID_KEY);
        $this->addHeader($requestIdKey, $requestId);
        $this->addHeader($requestIdKey, $requestId);
        $this->setRequestTime();
        return $requestId;
    }

    /**
     * @param bool $autoMake
     * @return mixed|null|string
     * @desc 获取请求id
     */
    public function getRequestId($autoMake = false)
    {
        $requestId = $this->getHeader(self::REQUEST_ID_KEY);
        if ($autoMake && empty($requestId)) {
            $requestId = self::makeRequestId();
        }
        return $requestId;
    }

    /**
     * @param $key
     * @return mixed|null
     * @desc 跟据key获取请求头信息
     */
    public function getHeader($key)
    {
        if (!empty($this->_headers[$key])) {
            return $this->_headers[$key];
        }

        if (self::isLongServer()) {
            if (self::isHttp() && $this->_request) {
                if (!empty($this->_request->header[$key])) {
                    return $this->_request->header[$key];
                }
            }
        } else {
            $key = 'HTTP_' . strtoupper(str_replace('-', '_', $key));
            if (!empty($_SERVER[$key])) {
                return $_SERVER[$key];
            }
        }
        return null;
    }

    /**
     * @return int
     * @desc 是否长驻服务 (swoole模式)
     */
    public static function isLongServer()
    {
        return self::$_long_server;
    }

    /**
     * @return int
     * @desc 是否http请求
     */
    public static function isHttp()
    {
        return self::$_is_http;
    }

    /**
     * @return string
     * @desc 生成请求id
     */
    public static function makeRequestId()
    {
        return sha1(uniqid('_' . mt_rand(1, 1000000), true));
    }

    /**
     * @param $key
     * @param $val
     * @desc 添加一个请求头
     */
    public function addHeader($key, $val)
    {
        $this->_headers[$key] = $val;
    }

    /**
     * @param $key
     * @param $val
     * @param bool $set
     * @desc 批量添加请求参数
     */
    public function addParams($key, $val, $set = true)
    {
        if ($set || !isset($this->_params[$key])) {
            $this->_params[$key] = $val;
        }
    }

    /**
     * @return mixed
     * @desc 获取请求参数数组
     */
    public function getParams()
    {
        return $this->_params;
    }

    /**
     * @param $params
     * @desc 设置请求参数数组
     */
    public function setParams($params)
    {
        $this->_params = $params;
    }

    /**
     * @return null
     * @desc 获取view模式
     */
    public function getViewMode()
    {
        return $this->_view_mode;
    }

    /**
     * @param $viewMode
     * @desc 设置view模式
     */
    public function setViewMode($viewMode)
    {
        $this->_view_mode = $viewMode;
    }

    /**
     * @return null
     * @desc 获取fd (swoole模式)
     */
    public function getFd()
    {
        return $this->_fd;
    }

    /**
     * @param $fd
     * @desc 设置fd (swoole模式)
     */
    public function setFd($fd)
    {
        $this->_fd = $fd;
    }

    /**
     * @return bool
     * @desc 是否ajax请求
     */
    public function isAjax()
    {

        if (!empty($this->_params['ajax'])) {
            return true;
        }
        if (self::isLongServer() && self::isHttp() && self::$_request
            && isset(self::$_request->header['X-Requested-With'])
            && 'xmlhttprequest' == strtolower(self::$_request->header['X-Requested-With']
            )
        ) {
            return true;
        }
        if ((isset($_SERVER['HTTP_X_REQUESTED_WITH'])
            && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
        ) {
            return true;
        }
        $field = QYSConfig::getField('project', 'jsonp', 'jsoncallback');
        if (self::isLongServer() && self::isHttp() && isset(self::$_request->header[$field])) {
            return true;
        }

        if (!empty($_REQUEST[$field])) {
            return true;
        }
        return false;
    }

    /**
     * @return null
     * @desc 获取http_request对像(swoole模式)
     */
    public function getRequest()
    {
        return $this->_request;
    }

    /**
     * @param $request
     * @desc 设置http_request对像(swoole模式)
     */
    public function setRequest($request)
    {
        $this->_request = $request;
        if ($request) {
            $this->_request_time = 0;
        }
    }

    /**
     * @return mixed
     * @desc 获取请求方法名
     */
    public function getRequestMethod()
    {
        if (self::isLongServer() && self::isHttp() && $this->_request) {
            if (isset($this->_request->header['request_method'])) {
                return $this->_request->header['request_method'];
            } elseif (isset($this->_request->server['request_method'])) {
                return $this->_request->server['request_method'];
            }
        }
        return $_SERVER['REQUEST_METHOD'];
    }

    /**
     * @return string
     * @desc 获取pathinfo
     */
    public function getPathInfo()
    {
        if (self::isLongServer() && self::isHttp() && $this->_request) {
            return isset($this->_request->server['path_info']) ? $this->_request->server['path_info'] : '';
        }
        return isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : '';
    }

    /**
     * @return string
     * @desc 获取客户端ip
     */
    public function getClientIp()
    {
        $realip = '';
        if (self::isLongServer()) {
            if (self::isHttp() && $this->_request) {
                $key = QYSConfig::getField('project', 'clientIpKey', 'X-Forwarded-For');
                if (isset($this->_request->header[$key])) {
                    $realip = $this->_request->header[$key];
                } else if (isset($this->_request->header["remote_addr"])) {
                    $realip = $this->_request->header["remote_addr"];
                }
            } else {
                if (self::$_fd) {
                    $connInfo = $this->getSocket()->connection_info(self::$_fd);
                    return $connInfo['remote_ip'];
                }
            }
        } else {
            $key = QYSConfig::getField('project', 'clientIpKey', 'HTTP_X_FORWARDED_FOR');
            if (isset($_SERVER[$key])) {
                $realip = $_SERVER[$key];
            } else if (isset($_SERVER["REMOTE_ADDR"])) {
                $realip = $_SERVER["REMOTE_ADDR"];
            }
        }

        return $realip;

    }

    /**
     * @return \swoole_server
     * @desc 获取swoole_server对像(swoole模式)
     */
    public function getSocket()
    {
        return $this->_socket;
    }

    /**
     * @param $socket
     * @desc 设置swoole_server对像(swoole模式)
     */
    public function setSocket($socket)
    {
        $this->_socket = $socket;
    }

    /**
     * @param array $headers
     * @param bool $init //是否初始化
     * @param bool $set //是否覆盖
     * @return array
     * @desc 添加一批请求头
     */
    public function addHeaders(array $headers, $init = false, $set = false)
    {
        if ($init) {
            $this->_headers = $headers;
        } else {
            if ($set) {
                $this->_headers = $headers + $this->_headers;
            } else {
                $this->_headers += $headers;
            }
        }
        return $this->_headers;
    }

    /**
     * @return array
     * @desc 获取所有待发的请求头
     */
    public function getHeaders()
    {
        return $this->_headers;
    }

    /**
     * @param bool $clear
     * @return null
     * @desc 获取请求开始时间
     */
    public function getRequestTime($clear = false)
    {
        $time = $this->_request_time;
        if ($clear) {
            $this->_request_time = 0;
        }
        return $time;
    }

    /**
     * @param null $time
     * @return bool
     */
    public function setRequestTime($time = null)
    {
        if (!empty($this->_request_time)) {
            return false;
        }
        if (empty($time)) {
            if (!empty($_REQUEST['REQUEST_TIME_FLOAT'])) {
                $time = $_REQUEST['REQUEST_TIME_FLOAT'];
            } else {
                $time = microtime(true);
            }
        }
        $this->_request_time = $time;
        $key = QYSConfig::getField('project', 'request_time_key', self::REQUEST_TIME_KEY);
        $this->addHeader($key, $time);
        return true;
    }

    public function getHttpMethod()
    {
        if (self::isLongServer() && self::isHttp() && $this->_request) {
            return isset($this->_request->server['request_method']) ? $this->_request->server['request_method'] : '';
        }
        return isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : 'GET';
    }


    public function getString($key)
    {
        $g = $this->_request->get;
        $val = !empty($g[$key]) ? $g[$key] : null;
        return $val;
    }

    public function getInt($key)
    {
        $g = $this->_request->get;
        $val = isset($g[$key]) ? $g[$key] : null;
        if (is_numeric($val)) {
            return \intval($val);
        }
        return null;
    }

    public function postArray($key)
    {
        $g = $this->_request->post;
        $array = !empty($g["$key"]) ? $g["$key"] : null;
        if (is_array($array)) {
            return $array;
        }
        $g = $this->_request->rawContent();
        if (is_null(json_decode($g))) {
            return null;
        }
        $g = json_decode($g, true);
        $array = !empty($g["$key"]) ? $g["$key"] : null;
        return $array;
    }

    public function postString()
    {
        $d = $this->_request->post;
        foreach (func_get_args() as $v) {
            $k = $v;
            if (!isset($d[$k])) {
                $d = null;
                break;
            }
            $d = $d[$k];
        }
        if (isset($d)) {
            return $d;
        }
        $g = $this->_request->rawContent();
        if (is_null(json_decode($g))) {
            return null;
        }
        $d = json_decode($g, true);
        foreach (func_get_args() as $v) {
            $k = $v;
            if (!isset($d[$k])) {
                $d = null;
                break;
            }
            $d = $d[$k];
        }
        return $d;
    }

    public function postInt()
    {
        $d = $this->_request->post;
        foreach (func_get_args() as $v) {
            $k = $v;
            if (!isset($d[$k])) {
                $d = null;
                break;
            }
            $d = $d[$k];
        }
        if (isset($d) && is_numeric($d)) {
            return \intval($d);
        }
        $g = $this->_request->rawContent();
        if (is_null(json_decode($g))) {
            return null;
        }
        $d = json_decode($g, true);
        foreach (func_get_args() as $v) {
            $k = $v;
            if (!isset($d[$k])) {
                $d = null;
                break;
            }
            $d = $d[$k];
        }
        if (isset($d) && is_numeric($d)) {
            return \intval($d);
        }
        return null;
    }

    // public function postString($key)
    // {
    //     $g=$this->_request->post;
    //     $val = !empty($g["$key"]) ? $g["$key"] : null;
    //     return $val;
    // }

    // public function postInt($key)
    // {
    //     $g=$this->_request->post;
    //     $val = isset($g["$key"]) ? $g["$key"] : null;
    //     if(is_numeric($val)){
    //         return \intval($val);
    //     }
    //     return null;
    // }

    /**
     * 获得raw content的数据作为一个hash表来处理
     */
    public function postContentArray($key)
    {
        try {
            $g = $this->_request->rawContent();
            $a = json_decode($g);
            if (isset($a->$key)) {
                if (is_array($a->$key)) {
                    return $a->$key;

                } else if (is_object($a->$key)) {
                    $b = array();
                    foreach ($a->$key as $key => $value) {
                        $b[$key] = $value;
                    }
                    return $b;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } catch (Exception $e) {
            return null;
        }
    }

    public function postContentInt($key)
    {
        try {
            $g = $this->_request->rawContent();
            $a = json_decode($g);
            if (isset($a->$key)) {
                $val = $a->$key;
                if (is_numeric($val)) {
                    return \intval($val);
                }
                return null;
            } else {
                return null;
            }
        } catch (Exception $e) {
            return null;
        }
    }

    public function postContentString($key)
    {
        try {
            $g = $this->_request->rawContent();
            $a = json_decode($g);
            if (isset($a->$key)) {
                return $a->$key;
            } else {
                return null;
            }
        } catch (Exception $e) {
            return null;
        }
    }
}