<?php

namespace QYS;

use Exception;

class HttpException extends Exception
{
    private array $data;

    public function __construct($message = '', $code = 10001, array $data = [])
    {
        $this->data = $data;
        parent::__construct($message, $code);
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }
}
