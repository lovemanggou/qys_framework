<?php
/**
 * Created by PhpStorm.
 * User: xhkj
 * Date: 2018/4/24
 * Time: 下午12:20
 */

namespace QYS\Db;

use QYS\Core\Config;

class Redis
{
    private static $instances = array();

    public static function put($name, $rediscli)
    {
        self::$instances[$name]->put($rediscli);
    }

    public static function getInstance($name)
    {
        $keyName = $name;
        if (isset(self::$instances[$name])) {
            // return self::$instances[$keyName];
            $rediscli = self::$instances[$keyName]->get();
            return new RedisWrapper($name, $rediscli);
        }

        $redisconig = Config::get("redis");

        if (empty($redisconig[$name])) {
            throw new \Exception(" 找不到redis数据的配置{$name}");
        }
        $dbconfig = $redisconig[$name];

        $host = $dbconfig['host'];
        $port = 6379;
        if (!empty($dbconfig['port'])) {
            $port = $dbconfig['port'];
        }

        $poolsize = 64;
        if (!empty($dbconfig['poolsize'])) {
            $poolsize = $dbconfig['poolsize'];
        }

        $pool = new \Swoole\Database\RedisPool((new \Swoole\Database\RedisConfig)
            ->withHost($host)
            ->withPort($port)
            , $poolsize);
        self::$instances[$keyName] = $pool;
        $rediscli = self::$instances[$keyName]->get();
        return new RedisWrapper($name, $rediscli);
    }

    public static function newInstance($name)
    {
        $keyName = $name;
        $redisconig = Config::get("redis");

        if (empty($redisconig[$name])) {
            throw new \Exception(" 找不到redis数据的配置{$name}");
        }
        $dbconfig = $redisconig[$name];

        $host = $dbconfig['host'];
        $port = 6379;
        if (!empty($dbconfig['port'])) {
            $port = $dbconfig['port'];
        }
        try {
            $redis = new \Redis();
            $redis->connect($host, $port, 500);
            return $redis;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}