<?php

namespace QYS\Db;

/**
 * @mixin \Redis
 */
class RedisWrapper
{
    private $redisCli = null;
    private $name = null;

    public function __construct($name, $rediscli)
    {
        $this->name = $name;
        $this->redisCli = $rediscli;
    }

    public function __destruct()
    {
        if ($this->redisCli) {
            Redis::put($this->name, $this->redisCli);
        }
    }

    public function __call($fname, $arguments)
    {
        return $this->redisCli->$fname(...$arguments);
    }
}