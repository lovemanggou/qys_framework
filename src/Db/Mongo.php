<?php
/**
 * Created by PhpStorm.
 * User: xhkj
 * Date: 2018/4/24
 * Time: 下午12:20
 */

namespace QYS\Db;

use MongoDB\Client;
use QYS\Core\Config;

class Mongo
{
    private static $instances = array();

    public static function getInstance($name)
    {
        $keyName = $name;
        if (isset(self::$instances[$name])) {
            return self::$instances[$keyName];
        }

//        $fun = dirname(QYS::getFrameWorkPath()) . DS . "MongoDB" . DS . "functions.php";
//        require_once $fun;

        $mongoconig = Config::get("mongo");

        if (empty($mongoconig[$name])) {
            throw new \Exception(" 找不到redis数据的配置{$name}");
        }
        $dbconfig = $mongoconig[$name];

        $host = $dbconfig['host'];
        $port = 27017;
        if (!empty($dbconfig['port'])) {
            $port = $dbconfig['port'];
        }

        try {
            $client = new Client("mongodb://$host:$port");
            self::$instances[$keyName] = $client;
        } catch (\Exception $e) {
            throw $e;
        }

        return self::$instances[$keyName];
    }
}