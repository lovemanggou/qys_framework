<?php

namespace QYS\Db;

use QYS\Core\Config;

class Mysql
{
    private static $pool = null;
    private static $instances = array();

    public static function put($name, $mycli)
    {
        self::$instances[$name]->put($mycli);
    }

    public static function getInstance($name)
    {
        $mysqlconfig = Config::get("mysql");

        if (empty($mysqlconfig[$name])) {
            throw new \Exception(" 找不到mysql数据的配置{$name}");
        }


        $dbconfig = $mysqlconfig[$name];

        $charset = "utf8mb4";
        if (!empty($dbconfig['charset'])) {
            $charset = $dbconfig['charset'];
        }

        $keyName = $name;
        if (isset(self::$instances[$name])) {
            // return self::$instances[$keyName];
            $mycli = self::$instances[$keyName]->get();

            $mycliwrapper = new MysqlWrapper($name, $mycli);
            $mycliwrapper->query("set names $charset");
            return $mycliwrapper;
        }

        $host = $dbconfig['host'];
        $user = $dbconfig['user'];
        $password = $dbconfig['password'];
        $database = $dbconfig['database'];

        $port = 3306;

        if (!empty($dbconfig['port'])) {
            $port = $dbconfig['port'];
        }

        $poolsize = 64;
        if (!empty($dbconfig['poolsize'])) {
            $poolsize = $dbconfig['poolsize'];
        }

        $pool = new \Swoole\Database\MysqliPool((new \Swoole\Database\MysqliConfig)
            ->withHost($host)
            ->withPort($port)
            ->withDbName($database)
            ->withCharset($charset)
            ->withUsername($user)
            ->withPassword($password)
            , $poolsize);

        self::$instances[$keyName] = $pool;

        $mycli = self::$instances[$keyName]->get();
        $mycliwrapper = new MysqlWrapper($name, $mycli);
        $mycliwrapper->query("set names $charset");
        return $mycliwrapper;
    }
}
