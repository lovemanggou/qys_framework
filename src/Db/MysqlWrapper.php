<?php

namespace QYS\Db;
use mysqli;

/**
 * @mixin mysqli
 */
class MysqlWrapper
{
    private $mysqli = null;
    private $name = null;

    public function __construct($name, $mysqli)
    {
        $this->name = $name;
        $this->mysqli = $mysqli;
    }

    public function __destruct()
    {
        if ($this->mysqli) {
            Mysql::put($this->name, $this->mysqli);
        }
    }

    public function __get($name)
    {
        return $this->mysqli->$name;
    }

    public function __call($fname, $arguments)
    {
        return $this->mysqli->$fname(...$arguments);
    }
}
