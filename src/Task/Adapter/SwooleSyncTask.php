<?php

namespace QYS\Task\Adapter;

use QYS\Protocol\Request;
use QYS\Task\ISyncTask;


class SwooleSyncTask implements ISyncTask
{
    private $config = NULL;
    private $task_id = NULL;

    public function __construct($config = null)
    {
        $this->config = $config;
        if (empty($this->config) || !is_array($this->config) || !array_key_exists('worker_id', $this->config)) {
            throw new \Exception("同步任务必须指定处理进程的ID");
        }
        if (empty($this->config) || !is_array($this->config) || empty($this->config['name'])) {
            throw new \Exception("必须指定任务的名字");
        }
    }

    public function run($serv)
    {
        $timeout = 0.5;
        $data = array();
        $dstWorkerId = -1;
        if (!empty($this->config) && is_array($this->config)) {
            if (!empty($this->config['timeout'])) {
                $tmp = $this->config['timeout'];
                if (is_float($tmp)) {
                    $timeout = $tmp;
                }
            }

            if (!empty($this->config['worker_id'])) {
                $tmp = $this->config['worker_id'];
                if (is_float($tmp)) {
                    $dstWorkerId = $tmp;
                }
            }

            if (!empty($this->config['data'])) {
                $data = $this->config;
            }
        }

        if (empty($serv)) {
            $serv = Request::getSocketServer();
        }

        return $serv->taskwait($data, $timeout, $dstWorkerId);
    }
}