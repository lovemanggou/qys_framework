<?php

namespace QYS\Task\Adapter;

use QYS\Protocol\Request;
use QYS\Task\IAsyncTask;

class SwooleAsyncTask implements IAsyncTask
{
    private $config = null;
    private $name = null;

    public function __construct($config = null)
    {
        $this->config = $config;
        if (empty($this->config) || !is_array($this->config) || empty($this->config['name'])) {
            throw new \Exception("必须指定任务的名字");
        }
    }

    public function run($serv)
    {
        $data = array();
        if (!empty($this->config) && is_array($this->config)) {
            if (!empty($this->config['data'])) {
                $data = $this->config;
            }
        }
        if (empty($serv)) {
            $serv = Request::getSocketServer();
        }

        $serv->task($data);
    }
}