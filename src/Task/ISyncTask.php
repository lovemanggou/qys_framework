<?php

namespace QYS\Task;

interface ISyncTask
{
    public function run($serv);
}