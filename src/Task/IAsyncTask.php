<?php

namespace QYS\Task;

interface IAsyncTask
{
    public function run($serv);
}