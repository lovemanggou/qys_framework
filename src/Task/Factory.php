<?php

namespace QYS\Task;

use QYS\Core\Factory as CFactory;

class Factory
{
    private static $_map = [
        'SwooleSyncTask' => 1,
        'SwooleAsyncTask' => 1,
    ];

    public static function builAsync($config = null)
    {
        return self::build("SwooleAsyncTask", $config);
    }

    public static function build($adapter = 'SwooleSyncTask', $config = null)
    {
//        $adapter = ucfirst(strtolower($adapter));

        if (isset(self::$_map[$adapter])) {
            $className = __NAMESPACE__ . "\\Adapter\\{$adapter}";
        } else {
            $className = $adapter;
        }
        return CFactory::build($className, $config);
    }

    public static function builSync($config = null)
    {
        return self::build("SwooleSyncTask", $config);
    }
}