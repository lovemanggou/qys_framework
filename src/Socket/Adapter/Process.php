<?php

namespace QYS\Socket\Adapter;

use QYS\Core\Config;
use QYS\Core\Factory as CFactory;
use QYS\Log\Log;
use QYS\Socket\IServer;
use Swoole\Process\Pool;

// use QYS\Util\Log;


class Process implements IServer
{
    private $clients = [];
    private $config;
    private $pool = null;

    public function __construct($config)
    {
        if (!\extension_loaded('swoole')) {
            throw new \Exception("no swoole extension. get: https://github.com/swoole/swoole-src");
        }
        $this->config = $config;
        // $this->workers = [];
    }


    public function setCallBack($client)
    {
        if (!is_object($client)) {
            throw new \Exception('client must object');
        }
        $this->client = $client;
        return true;
    }

    public function run()
    {
        \Swoole\Process::daemon();
        $processcnf = Config::get('process');
        $worker_num = $processcnf['worker_num'];
        $appname = Config::get("appname");

        $callback_class = $this->config['callback_class'];
        for ($i = 0; $i < $worker_num; $i++) {
            $this->clients[$i] = CFactory::build($callback_class);
        }

        $clients = $this->clients;
        $pool = new Pool($worker_num);
        $pool->set(['enable_coroutine' => true]);

        $pool->on("WorkerStart", function ($pool, $workerId) use ($clients, $worker_num) {
            Log::var_dump("Process Worker#$workerId is started\n");
            $client = $clients[$workerId];

            $process = $pool->getProcess();
            $client->onWorkerStart($process, $workerId, $worker_num);

            $running = true;
            pcntl_signal(SIGTERM, function () use (&$running) {
                $running = false;
            });
            while ($running) {
                $client->loop();
                pcntl_signal_dispatch();

            }
        });

        $pool->on("WorkerStop", function ($pool, $workerId) {
            Log::var_dump("Process Worker#$workerId stopped\n");
            $client = $this->clients[$workerId];
            $process = $pool->getProcess($workerId);
            $client->onWorkerStop($process);
        });
        $this->pool = $pool;
        $pool->start();
    }


}
