<?php

namespace QYS\Socket\Adapter;

use Exception;
use http\Client;
use QYS\QYS;
use QYS\Socket\Callback;
use QYS\Socket\IServer;
use Swoole\Http\Server as HttpServer;
use Swoole\Server;
use Swoole\WebSocket\Server as WebsocketServer;
use function extension_loaded;
use const SWOOLE_SOCK_TCP;
use const SWOOLE_SSL;

class Swoole implements IServer
{
    const TYPE_TCP = 'tcp';
    const TYPE_UDP = 'udp';
    const TYPE_HTTP = 'http';
    const TYPE_HTTPS = 'https';
    const TYPE_WEBSOCKET = 'ws';
    const TYPE_WEBSOCKETS = 'wss';
    private $client;
    private $config;
    /** @var HttpServer|Server|WebsocketServer */
    private $serv;

    /**
     * @throws Exception
     */
    public function __construct(array $config)
    {
        if (!extension_loaded('swoole')) {
            throw new Exception("no swoole extension. get: https://github.com/swoole/swoole-src");
        }
        $this->config = $config;
        $socketType = empty($config['server_type']) ? self::TYPE_TCP : strtolower($config['server_type']);
        $this->config['server_type'] = $socketType;
        $workMode = empty($config['work_mode']) ? SWOOLE_PROCESS : $config['work_mode'];

        $swoole_config = [];
        $ssl = 0;
        if (!empty($this->config['ssl_cert_file']) && !empty($this->config['ssl_key_file'])) {
            $ssl = SWOOLE_SSL;
        }
        switch ($socketType) {
            case self::TYPE_TCP:
                $this->serv = new Server($config['host'], $config['port'], $workMode, SWOOLE_SOCK_TCP | $ssl);
                break;
            case self::TYPE_UDP:
                $this->serv = new Server($config['host'], $config['port'], $workMode, SWOOLE_SOCK_UDP | $ssl);
                break;
            case self::TYPE_HTTP:
                $swoole_config = $this->config['options'];
                $this->serv = new HttpServer($config['host'], $config['port'], SWOOLE_PROCESS);
                break;
            case self::TYPE_HTTPS:
                if (!$ssl) {
                    throw new Exception("https must set ssl_cert_file && ssl_key_file");
                }
                $this->serv = new HttpServer($config['host'], $config['port'], SWOOLE_SOCK_TCP | SWOOLE_SSL);
                break;
            case self::TYPE_WEBSOCKET:
                $swoole_config = $this->config['options'];
                $this->serv = new WebsocketServer($config['host'], $config['port'], SWOOLE_PROCESS);
                break;
            case self::TYPE_WEBSOCKETS:
                if (!$ssl) {
                    throw new Exception("https must set ssl_cert_file && ssl_key_file");
                }
                $this->serv = new WebsocketServer($config['host'], $config['port'], $workMode, SWOOLE_SOCK_TCP | SWOOLE_SSL);
                break;
        }

        if (!empty($config['add_listen']) && $socketType != self::TYPE_UDP && SWOOLE_PROCESS == $workMode) {
            $this->serv->addlistener($config['add_listen']['ip'], $config['add_listen']['port'], $config['add_listen']['socket_type']);
        }
        $this->serv->set($swoole_config);
    }

    /**
     * @param Callback\Swoole $client
     * @return bool
     * @throws Exception
     * @author yin
     */
    public function setCallBack($client): bool
    {
        if (!is_object($client)) {
            throw new Exception('client must object');
        }
        switch ($this->config['server_type']) {
            case self::TYPE_WEBSOCKET:
            case self::TYPE_WEBSOCKETS:
                if (!($client instanceof Callback\SwooleWebSocket)) {
                    throw new Exception('client must instanceof QYS\Socket\Callback\SwooleWebSocket');
                }
                break;
            case self::TYPE_HTTP:
            case self::TYPE_HTTPS:
                if (!($client instanceof Callback\SwooleHttp)) {
                    throw new Exception('client must instanceof QYS\Socket\Callback\SwooleHttp');
                }
                break;
            case self::TYPE_UDP:
                if (!($client instanceof Callback\SwooleUdp)) {
                    throw new Exception('client must instanceof QYS\Socket\Callback\SwooleUdp');
                }
                break;
            default:
                if (!($client instanceof Callback\Swoole)) {
                    throw new Exception('client must instanceof QYS\Socket\Callback\Swoole');
                }
                break;
        }
        $this->client = $client;
        return true;
    }


    public function run()
    {
        $handlerArray = array(
            'onTimer',
            'onWorkerStop',
            'onWorkerError',
            'onTask',
            'onFinish',
            'onManagerStart',
            'onManagerStop',
            'onPipeMessage',
            'onPacket',
        );

        $this->serv->on('Start', array($this->client, 'onStart'));
        $this->serv->on('Shutdown', array($this->client, 'onShutdown'));

        $dispatch_mode = $this->config['dispatch_mode'];
        if (!in_array($dispatch_mode, [1, 3, 7])) {
            $this->serv->on('Connect', array($this->client, 'onConnect'));
        }

        if (!in_array($dispatch_mode, [1, 3, 7])) {
            $this->serv->on('Close', array($this->client, 'onClose'));
        }

        $this->serv->on("WorkerStart", function ($server, $workerId) {
            if ($workerId === 0) {
                $config_path = QYS::getProjPath();
                $pid = fopen($config_path . DIRECTORY_SEPARATOR . basename(QYS::getConfigPath()) . ".pid", "w");
                fwrite($pid, "" . $server->master_pid);
            }

            if (method_exists($this->client, 'onWorkerStart')) {
                $this->client->onWorkerStart($server, $workerId);
            }
        });
        switch ($this->config['server_type']) {
            case self::TYPE_TCP:
                $this->serv->on('Receive', array($this->client, 'doReceive'));
                break;
            case self::TYPE_HTTP:
            case self::TYPE_HTTPS:
                $this->serv->on('Request', array($this->client, 'doRequest'));
                break;
            case self::TYPE_WEBSOCKET:
            case self::TYPE_WEBSOCKETS:
                if (method_exists($this->client, 'onHandShake')) {
                    $this->serv->on('HandShake', array($this->client, 'onHandShake'));
                }
                if (method_exists($this->client, 'onOpen')) {
                    $this->serv->on('Open', array($this->client, 'onOpen'));
                }


                if (method_exists($this->client, 'doRequest')) {
                    $this->serv->on('Request', array($this->client, 'doRequest'));
                }
                $this->serv->on('Message', array($this->client, 'onMessage'));
                break;
            case self::TYPE_UDP:
                array_pop($handlerArray);
                $this->serv->on('Packet', array($this->client, 'doPacket'));
                break;
        }

        foreach ($handlerArray as $handler) {
            if (method_exists($this->client, $handler)) {
                $this->serv->on(\substr($handler, 2), array($this->client, $handler));
            }
        }
        $this->serv->start();
    }
}
