<?php

namespace QYS\Socket;

use QYS\Core\Config;
use QYS\Core\Factory as CFactory;

class Factory
{
    private static $_map = [
        'Swoole' => 1,
        'Process' => 1,
    ];

    public static function getInstance($adapter = 'Swoole', $config = null)
    {
        if (empty($config)) {
            $config = Config::get('socket');;
            if (!empty($config['adapter'])) {
                $adapter = $config['adapter'];
            }
        }
        $adapter = ucfirst(strtolower($adapter));
        if (isset(self::$_map[$adapter])) {
            $className = __NAMESPACE__ . "\\Adapter\\$adapter";
        } else {
            $className = $adapter;
        }
        return CFactory::getInstance($className, $config);
    }
}