<?php

namespace QYS\Socket;

interface IServer
{
    /**
     * 设置socket回调类
     */
    function setCallBack($client);

    /**
     * 运行socket服务
     */
    function run();
}