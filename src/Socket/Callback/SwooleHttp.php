<?php


namespace QYS\Socket\Callback;

use QYS\Core\Config;
use QYS\Core\Factory;
use QYS\Protocol;
use QYS\Waf\Waf;

abstract class SwooleHttp extends Swoole
{
    protected $beanpid;
    protected $taskpid;
    protected $queuepid;

    public function onReceive()
    {

    }

    public function onTask($server, $taskId, $fromId, $data)
    {
        $task = Config::get('task', array());
        try {
            $name = $data['name'];
            if (array_key_exists($name, $task)) {
                $class = Factory::getInstance($task[$name]);
                $method = $name;
                return $class->$method($server, $taskId, $fromId, $data);
            } else {

            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function onWorkerStart($server, $workerId)
    {
        Waf::init();
        parent::onWorkerStart($server, $workerId);
        Protocol\Request::setHttpServer(1);
        Protocol\Request::setSocketServer($server);
    }

    public function doRequest($request, $response)
    {
        $req = new Protocol\Request();
        $req->setRequest($request);
        $req_id = $req->getRequestId(true);
        $req->setRequestId($req_id);
        $resp = new Protocol\Response();
        $resp->setResponse($response);
        $resp->setRequest($req);
        $this->onRequest($req, $resp);
    }

    abstract public function onRequest($request, $response);
}
