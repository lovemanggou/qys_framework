<?php
/**
 * Created by PhpStorm.
 * User: xhkj
 * Date: 2018/4/26
 * Time: 下午4:33
 */

namespace QYS\Socket\Callback;


class SwooleTcp extends Swoole
{
    public function onPacket($server, $data, $clientInfo)
    {
        throw new \Exception('tcp server must use onReceive');
    }

    public function onReceive()
    {

    }

    public function onWorkerStart($server, $workerId)
    {
        parent::onWorkerStart($server, $workerId);
        if (!$server->taskworker) {
            $config = QConfig::get("channel_client");
            if (!empty($config)) {
                $c = new ChannelClient($server, 5);
                $c->setCallback($this);
                $c->connect($config['host'], $config['port'], $config['port']);
                $server->channel_client = $c;
            }
        }
    }
}