<?php


namespace QYS\Socket\Callback;

use QYS\Channel\ChannelClient;
use QYS\Core\Config as QConfig;

abstract class SwooleWebSocket extends Swoole
{
    public function onRequest($request, $response)
    {
    }

    public function onReceive()
    {

    }

    public function onCLose()
    {
    }

    public function onWorkerStart($server, $workerId)
    {
        parent::onWorkerStart($server, $workerId);
        if (!$server->taskworker) {
            $config = QConfig::get("channel_client");
            if (!empty($config)) {
                $c = new ChannelClient($server, 5);
                $c->setCallback($this);
                $c->connect($config['host'], $config['port'], $config['port'], $config['server_id']);
                $server->channel_client = $c;
            }
        }
    }

    abstract public function onMessage($server, $frame);
}
