<?php
/**
 * Created by PhpStorm.
 * User: xhkj
 * Date: 2018/4/27
 * Time: 下午3:47
 */

namespace QYS\Socket\Callback;


abstract class SwooleChannel extends Swoole
{
    protected $serv = null;
    protected $workerId = null;

    public function onPacket($server, $data, $clientInfo)
    {
        throw new \Exception('tcp server must use onReceive');
    }

    public function onReceive()
    {

    }

    final public function doReceive($server, $fd, $from_id, $data)
    {
        $package = unserialize(substr($data, 4));
        $c = $package['c'];
        $m = $package['m'];
        $sid = $package['sid'];
        $wid = $package['wid'];
        if ('CORE' == strtoupper($c)) {
            switch ($m) {
                case 'login':
                    $key = '' . $sid . '-' . $wid;
                    $server->key2fd->set($key, array('fd' => '' . $fd));
                    $server->fd2key->set('' . $fd, array('wid' => $wid, 'sid' => $sid));
                    break;
                default:
                    break;
            }
        } elseif ('CLIENT' == strtoupper($c)) {
            $this->onChannelReceive($fd, $sid, $wid, $package['data']);
        }
    }

    abstract function onChannelReceive($fd, $sid, $wid, $data);

    public function onClose()
    {
        $server = func_get_args()[0];
        $fd = func_get_args()[1];
        $t = $this->serv->fd2key->get('' . $fd);
        if (!empty($t)) {
            $key = $t['sid'] . '-' . $t['wid'];
            $server->key2fd->del($key);
            $server->fd2key->del('' . fd);
        }
    }

    public function sendFd($fd, $data)
    {
        $this->innersend($this->serv, $fd, 'client', 'sendmessage', $data);
    }

    private function innersend($serv, $fd, $c, $m, $data)
    {
        $package = array('c' => $c, 'm' => $m, 'data' => $data);
        $sendStr = serialize($package);
        $sendData = pack('N', strlen($sendStr)) . $sendStr;
        $serv->send($fd, $sendData);
    }

    public function send($sid, $wid, $data)
    {
        $key = '' . $sid . '-' . $wid;
        $t = $this->serv->key2fd->get($key);
        if (!empty($t)) {
            $this->innersend($this->serv, $t['fd'], 'client', 'sendmessage', $data);
        }
    }

    public function onManagerStart($server)
    {
//        $count = 102;
//        $table = new \swoole_table($count);
//        $table->column('fd', \swoole_table::TYPE_STRING,64);
//        $table->create();
//        $server->key2fd=$table;
//
//        $table1 = new \swoole_table($count);
//        $table1->column('wid', \swoole_table::TYPE_INT,64);
//        $table1->column('sid', \swoole_table::TYPE_STRING,16);
//        $table1->create();
//        $server->fd2key=$table1;
    }

    public function onWorkerStart($server, $workerId)
    {
        parent::onWorkerStart($server, $workerId);
        if (!$server->taskworker) {
            $this->workerId = $workerId;
            $this->serv = $server;
        }

    }
}