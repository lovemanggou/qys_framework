<?php


namespace QYS\Socket\Callback;

use QYS\Log\Log;
use QYS\Socket\ICallback;
use Swoole\Runtime;


abstract class Swoole implements ICallback
{

    protected $protocol;

    protected $serv;

    /**
     * @throws \Exception
     * @desc 服务启动，设置进程名及写主进程id
     */
    public function onStart()
    {
    }

    /**
     * @throws \Exception
     */
    public function onShutDown()
    {

    }

    /**
     * @param $server
     * @throws \Exception
     * @desc 服务启动，设置进程名
     */
    public function onManagerStart($server)
    {

    }

    /**
     * @param $server
     * @throws \Exception
     * @desc 服务关闭，删除进程id文件
     */
    public function onManagerStop($server)
    {
    }

    public function onWorkerStart($server, $workerId)
    {
        Runtime::enableCoroutine($flags = SWOOLE_HOOK_ALL);
        var_dump('callback swoole server start [' . $workerId . ']' . PHP_EOL);
    }

    public function onWorkerStop($server, $workerId)
    {
    }

    public function onWorkerError($server, $workerId, $workerPid, $errorCode, $signal)
    {

    }


    public function onConnect()
    {

    }

    public function doReceive($server, $fd, $from_id, $data)
    {

    }

    abstract public function onReceive();

    public function onPacket($server, $data, $clientInfo)
    {

    }

    public function onClose()
    {
    }


    public function onTask($server, $taskId, $fromId, $data)
    {

    }

    public function onFinish($server, $taskId, $data)
    {

    }

    public function onPipeMessage($server, $fromWorerId, $data)
    {

    }


}
