<?php

namespace QYS\View\Adapter;

use QYS\Protocol\Request;
use QYS\Protocol\Response;
use QYS\View\Base;

class Str extends Base
{
    public function display()
    {
        if (Request::isHttp()) {
            Response::sendHttpHeader();
            Response::header("Content-Type", "text/plain; charset=utf-8");
        }

        if (\is_array($this->model) || \is_object($this->model)) {
            $data = json_encode($this->model);
        } else {
            $data = $this->model;
        }
        if (Request::isLongServer()) {
            return $data;
        }

        echo $data;
        return null;
    }
}