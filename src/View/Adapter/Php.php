<?php

namespace QYS\View\Adapter;

use QYS\Protocol\Request;
use QYS\Protocol\Response;
use QYS\View\Base;

class Php extends Base
{
    private $tplFile;

    public function setTpl($tpl)
    {
        $this->tplFile = $tpl;
    }

    public function display()
    {
        Response::sendHttpHeader();
        $tplPath = QYS\Core\Config::getField('project', 'tpl_path', QYS\QYS::getRootPath() . DS . 'template' . DS . 'default' . DS);
        $fileName = $tplPath . $this->tplFile;
        if (!\is_file($fileName)) {
            throw new \Exception("no file {$fileName}");
        }
        if (!empty($this->model) && is_array($this->model)) {
            \extract($this->model);
        }
        if (Request::isLongServer()) {
            \ob_start();
            include "{$fileName}";
            $content = ob_get_contents();
            \ob_end_clean();
            return $content;
        }
        include "{$fileName}";
        return null;
    }


}
