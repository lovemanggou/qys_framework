<?php

namespace QYS\View;

abstract class Base implements IView
{

    protected $model;

    public function getModel()
    {
        return $this->model;
    }

    public function setModel($model)
    {
        $this->model = $model;
    }

    //数据输出

    public function render()
    {
        \ob_start();
        $this->display();
        $content = \ob_get_contents();
        \ob_end_clean();
        return $content;
    }

    abstract public function display();

}