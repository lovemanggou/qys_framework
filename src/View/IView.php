<?php

namespace QYS\View;

interface IView
{
    //存入数据
    public function setModel($model);

    //获取数据
    public function getModel();

    //渲染数据
    public function render();
}
