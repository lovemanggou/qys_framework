<?php

namespace QYS\Log;

use QYS\Core\Config;
use QYS\QYS;
use QYS\Util\Dir;
use Swoole\Coroutine as co;

class Log
{
    const SEPARATOR = "\t";

    private static $xhprof = false;
    private static $records;

    private static $DEBUG_TRACE = false;

    public static function getMicroTime()
    {
        list($usec, $sec) = \explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }

    public static function access($request, $response)
    {
        $t = \date("Ymd");
//        var_dump($request->getRequest());

        $r = $request->getRequest();
        $remote_addr = $r->server["remote_addr"];
        $method = $r->server["request_method"];
        $params = null;
        if (strtoupper($method) == "GET") {
            $params = array(
                $r->server["request_method"],
                $r->get,
                $r->header["user-agent"],
            );
        } else {
            $params = array(
                $r->server["request_method"],
                $r->post,
                $r->header["user-agent"],
            );
        }

        $logPath = Config::getField('project', 'log_path', '');
        if (empty($logPath)) {
            $dir = QYS::getProjPath() . DS . 'log' . DS . $t;
        } else {
            $dir = $logPath . DS . $t;
        }
        Dir::make($dir);
        $requestId = $request->getRequestId();
        $str = $remote_addr . self::SEPARATOR . \date('Y-m-d H:i:s', Config::get('now_time', time())) . self::SEPARATOR . $request->getPathInfo() . self::SEPARATOR . $requestId . self::SEPARATOR . \implode(self::SEPARATOR, array_map('QYS\Log\Log::myJson', $params));
        $logFile = $dir . \DS . "access" . '.log';
        co::create(function () use ($logFile, $str) {
            $r = co::writeFile($logFile, $str . "\n", FILE_APPEND);
        });

    }

    public static function info($type, $request, $params = array())
    {

        $t = \date("Ymd");
        $logPath = Config::getField('project', 'log_path', '');
        if (empty($logPath)) {
            $dir = QYS::getProjPath() . DS . 'log' . DS . $t;
        } else {
            $dir = $logPath . DS . $t;
        }
        Dir::make($dir);
        $requestId = $request->getRequestId();
        $str = \date('Y-m-d H:i:s', Config::get('now_time', time())) . self::SEPARATOR . $request->getPathInfo() . self::SEPARATOR . $requestId . self::SEPARATOR . \implode(self::SEPARATOR, array_map('QYS\Log\Log::myJson', $params));
        $logFile = $dir . \DS . $type . '.log';
//        \file_put_contents($logFile, $str . "\n", FILE_APPEND | LOCK_EX);
        co::create(function () use ($logFile, $str) {
            $r = co::writeFile($logFile, $str . "\n", FILE_APPEND);
        });

    }

    public static function exception($exception)
    {
        $type = "exception";
        $t = \date("Ymd");
        $logPath = Config::getField('project', 'log_path', '');
        if (empty($logPath)) {
            $dir = QYS::getProjPath() . DS . 'log' . DS . $t;
        } else {
            $dir = $logPath . DS . $t;
        }
        Dir::make($dir);
        $str = \date('Y-m-d H:i:s', Config::get('now_time', time())) . self::SEPARATOR . self::SEPARATOR . self::SEPARATOR . self::SEPARATOR . $exception->getMessage();
        $logFile = $dir . \DS . $type . '.log';
        \file_put_contents($logFile, $str . "\n", FILE_APPEND | LOCK_EX);

//        co::create(function () use ($logFile,$str)
//        {
//            co::writeFile($logFile, $str . "\n",FILE_APPEND);
//        });
    }

    public static function dump($params = array())
    {
        $type = "dump";
        $t = \date("Ymd");
        $logPath = Config::getField('project', 'log_path', '');
        if (empty($logPath)) {
            $dir = QYS::getProjPath() . DS . 'log' . DS . $t;
        } else {
            $dir = $logPath . DS . $t;
        }
        Dir::make($dir);
        $str = \date('Y-m-d H:i:s', Config::get('now_time', time())) . self::SEPARATOR . \implode(self::SEPARATOR, array_map('QYS\Log\Log::myJson', $params));
        $logFile = $dir . \DS . $type . '.log';
        \file_put_contents($logFile, $str . "\n", FILE_APPEND | LOCK_EX);
    }

    public static function var_dump($a)
    {
        $trace = debug_backtrace();
        $caller0 = $trace[0];
        $caller = $trace[1];

        if (isset($caller['class'])) {
            $c = $caller['class'];
            $f = $caller['function'];
            $file = $caller0['file'];

            $l = $caller0['line'];
            print_r("dump from: class $c ($file): in function '$f' line $l" . PHP_EOL);
            var_dump($a);
        } else {
            $f = $caller['function'];
            $l = $caller['line'];
            $file = $caller0['file'];
            $l = $caller0['line'];
            print_r("dump from ($file): in function '$f' in line $l" . PHP_EOL);
            var_dump($a);
        }
    }

    public static function echo($a)
    {
        $trace = debug_backtrace();
        $caller0 = $trace[0];
        $caller = $trace[1];

        if (isset($caller['class'])) {
            $c = $caller['class'];
            $f = $caller['function'];
            $file = $caller0['file'];

            $l = $caller0['line'];
            print_r("dump from: class $c ($file): in function '$f' line $l" . PHP_EOL);
            echo $a;
        } else {
            $f = $caller['function'];
            $l = $caller['line'];
            $file = $caller0['file'];
            $l = $caller0['line'];
            print_r("dump from ($file): in function '$f' in line $l" . PHP_EOL);
            echo $a;
        }
    }

    public static function print_r($a)
    {
        if (self::$output_dump) {
            $trace = debug_backtrace();
            $caller0 = $trace[0];
            $caller = $trace[1];

            if (isset($caller['class'])) {
                $c = $caller['class'];
                $f = $caller['function'];
                $file = $caller0['file'];

                $l = $caller0['line'];
                print_r("dump from: class $c ($file): in function '$f' line $l" . PHP_EOL);
                print_r($a);
            } else {
                $f = $caller['function'];
                $l = $caller['line'];
                $file = $caller0['file'];
                $l = $caller0['line'];
                print_r("dump from ($file): in function '$f' in line $l" . PHP_EOL);
                print_r($a);
            }
        } else {

        }
    }

    public static function myJson($data)
    {
        return json_encode($data, JSON_UNESCAPED_UNICODE);
    }
}
