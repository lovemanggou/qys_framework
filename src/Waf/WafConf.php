<?php

namespace QYS\Waf;

class WafConf
{
    /**
     */
    public static function get_args_config()
    {
        static $conf = array(
            "\.\.",
            "\:\$",
            "\$\{",
            "select.+(from|limit)",
            "(?:(union(.*?)select))",
            "having|rongjitest",
            "sleep\((\s*)(\d*)(\s*)\)",
            "benchmark\((.*)\,(.*)\)",
            "base64_decode\(",
            "(?:from\W+information_schema\W)",
            "(?:(?:current_)user|database|schema|connection_id)\s*\(",
            "(?:etc\/\W*passwd)",
            "into(\s+)+(?:dump|out)file\s*",
            "group\s+by.+\(",
            "xwork.MethodAccessor",
            "(?:define|eval|file_get_contents|include|require|require_once|shell_exec|phpinfo|system|passthru|preg_\w+|execute|echo|print|print_r|var_dump|(fp)open|alert|showmodaldialog)\(",
            "xwork\.MethodAccessor",
            "(gopher|doc|php|glob|file|phar|zlib|ftp|ldap|dict|ogg|data)\:\/",
            "java\.lang",
            "\$_(GET|post|cookie|files|session|env|phplib|GLOBALS|SERVER)\[",
            "\<(iframe|script|body|img|layer|div|meta|style|base|object|input)",
            "(onmouseover|onerror|onload)\=",
            "(\bEXEC\b|\bAND\b|\bOR\b|\bJOIN\b|\bDELETE\b)",
            "(\bexec\b|\band\b|\bor\b|\bjoin\b|\bdelete\b)",
            "union.+?select|update.+?set|insert\\s+into.+?values|(select|delete).+?from|(create|alter|drop|truncate)\\s+(table|database)",
            "UNION.+?SELECT|UPDATE.+?SET|INSERT\\s+INTO.+?VALUES|(SELECT|DELETE).+?FROM|(CREATE|ALTER|DROP|TRUNCATE)\\s+(TABLE|DATABASE)"
        );

        return $conf;
    }

    /**
     */
    public static function get_url_config()
    {
        static $conf = array(
            "\.(svn|htaccess|bash_history|ssh|git|sql)",
            "\.(bak|inc|old|mdb|sql|backup|java|class)$",
            "(vhost|bbs|host|wwwroot|www|site|root|hytop|flashfxp).*\.rar",
            "(phpmyadmin|jmx-console|jmxinvokerservlet)",
            "java\.lang",
            "\\/(attachments|upimg|images|css|uploadfiles|html|uploads|templets|static|template|data|inc|forumdata|upload|includes|cache|avatar)\\/(\w+).(php|jsp)",

        );
        return $conf;
    }

    /**
     */
    public static function get_post_config()
    {
        static $conf = array(
            "select.+(from|limit)",
            "(?:(union(.*?)select))",
            "having|rongjitest",
            "sleep\((\s*)(\d*)(\s*)\)",
            "benchmark\((.*)\,(.*)\)",
            "base64_decode\(",
            "(?:from\W+information_schema\W)",
            "(?:(?:current_)user|database|schema|connection_id)\s*\(",
            "(?:etc\/\W*passwd)",
            "into(\s+)+(?:dump|out)file\s*",
            "group\s+by.+\(",
            "xwork.MethodAccessor",
            "(?:define|eval|file_get_contents|include|require|require_once|shell_exec|phpinfo|system|passthru|preg_\w+|execute|echo|print|print_r|var_dump|(fp)open|alert|showmodaldialog)\(",
            "xwork\.MethodAccessor",
            "(gopher|doc|php|glob|file|phar|zlib|ftp|ldap|dict|ogg|data)\:\/",
            "java\.lang",
            "\$_(GET|post|cookie|files|session|env|phplib|GLOBALS|SERVER)\[",
            "\<(iframe|script|body|img|layer|div|meta|style|base|object|input)",
            "(onmouseover|onerror|onload)\=",
            "(\bEXEC\b|\bAND\b|\bOR\b|\bJOIN\b|\bDELETE\b)",
            "(\bexec\b|\band\b|\bor\b|\bjoin\b|\bdelete\b)",
            "union.+?select|update.+?set|insert\\s+into.+?values|(select|delete).+?from|(create|alter|drop|truncate)\\s+(table|database)",
            "UNION.+?SELECT|UPDATE.+?SET|INSERT\\s+INTO.+?VALUES|(SELECT|DELETE).+?FROM|(CREATE|ALTER|DROP|TRUNCATE)\\s+(TABLE|DATABASE)",
        );

        return $conf;
    }
}
