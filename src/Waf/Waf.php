<?php

namespace QYS\Waf;

use QYS\QYS;


class Waf
{
    private static $blackfile_Ext = array(".php", ".jsp");
    private static $urlrules = null;
    private static $argsrules = null;
    private static $postrules = null;

    public static function init()
    {
        self::$urlrules = WafConf::get_url_config();
        self::$argsrules = WafConf::get_args_config();
        self::$postrules = WafConf::get_post_config();
    }

    public static function access($request, $response)
    {
        if (isset($r->server["HTTP_ACUNETIX_ASPECT"])) {
            self::saybye($request, $response);
        } elseif (isset($r->server["HTTP_X_SCAM_MEMO"])) {
            self::saybye($request, $response);
        } elseif (self::url($request, $response)) {
            self::saybye($request, $response);
        } elseif (self::args($request, $response)) {
            self::saybye($request, $response);
        } else {
            $method = $request->getRequestMethod();
            if (strtoupper($method) == "POST") {
                if (self::get_boundary($request, $response)) {
                    if (isset($r->post)) {
                        $p = $r->post;
                        foreach ($p as $key => $val) {
                            $data = null;
                            if (gettype($val) == "array") {
                                if ($val[0] == "boolean") {
                                    return;
                                }
                                $data = join(", ", $val);
                            } else {
                                $data = $val;
                            }

                            if (isset($data) && gettype($data) != "boolean") {
                                if (self::body($key) || self::body($data)) {
                                    self::saybye($request, $response);
                                    return;
                                }
                            }
                        }
                    } else {
                        if (isset($r->files)) {
                            $files = $r->files;
                            $file = $files["file"];
                            $fname = $file["name"];
                            $fext = pathinfo($fname, PATHINFO_EXTENSION);

                            if (strtoupper($fext) == "PHP" || strtoupper($fext) == "JSP" || strtoupper($fext) == "SH") {
                                self::saybye($request, $response);
                                return;
                            }

                            if (isset($r->tmpfiles)) {
                                $myfile = fopen($r->tmpfiles[0], "r");
                                if ($myfile) {
                                    $content = fread($myfile, filesize($r->tmpfiles[0]));
                                    if (self::body($content)) {
                                        self::saybye($request, $response);
                                        return;
                                    }
                                }
                            }
                        }

                    }
                } else {
                    if (isset($r->post)) {
                        $p = $r->post;
                        foreach ($p as $key => $val) {
                            $data = null;
                            if (gettype($val) == "array") {
                                if ($val[0] == "boolean") {
                                    return;
                                }
                                $data = join(", ", $val);
                            } else {
                                $data = $val;
                            }

                            if (isset($data) && gettype($data) != "boolean") {
                                if (self::body($key) || self::body($data)) {
                                    self::saybye($request, $response);
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private static function saybye($request, $response)
    {
        $response->addHeader("Content-Type", 'application/json');
        $response->sendHttpHeader();
        $response->status(403);
        $response->say(json_encode(array(
                "errcode" => 403,
                "errmsg" => "WAF已阻止了相关操作!!!请不要使用使用特殊关键词"))
        );
        QYS::bye();
    }

    public static function url($request, $response)
    {
        $r = $request->getRequest();

        if (isset($r->server) && isset($r->server["request_uri"])) {
            $request_uri = $r->server["request_uri"];
            foreach (self::$urlrules as $rule) {
                if ($rule != "" and preg_match("/" . $rule . "/is", $request_uri)) {
                    return true;
                }
            }
        }

        return false;
    }

    public static function args($request, $response)
    {
        $r = $request->getRequest();
        if (isset($r->get)) {
            $args = $r->get;
            foreach (self::$argsrules as $rule) {
                foreach ($args as $key => $val) {
                    $data = null;
                    if (gettype($val) == "array") {
                        $t = array();
                        foreach ($val as $k => $v) {
                            if ($v == true) {
                                $v = "";
                            }
                            array_push($t, $v);
                        }

                        $v = join(" ", $t);
                    } else {
                        $data = $val;
                    }
                    if (isset($data) && gettype($data) != "boolean" and $rule != "" and preg_match("/" . $rule . "/is", urldecode($data))) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private static function get_boundary($request, $response)
    {

        $r = $request->getRequest();
        if (isset($r->header)) {

            $header = $r->header;

            if (!isset($header["content-type"])) {
                return false;
            }
            $content_type = $header["content-type"];


            if (gettype($content_type) == "array") {
                $header = $header[0];
            }


            preg_match('/boundary=(.*)$/', $content_type, $matches);

            if (sizeof($matches) > 0) {
                return true;
            }

            return false;
//            Debug::info($matches);
//            $rule = ";%s*boundary=";
//            $m = preg_match("/".$rule."/is",$content_type);
//            Debug::info(array("m"=>"$m","content_type"=>$content_type,"rule"=>$rule));
//            if($m){
//                return true;
//            }
//
//            $rule = ";%s*boundary=\"([^\"]+)\"";
//            return preg_match("/".$rule."/is",$content_type);
        } else {
            return false;
        }
    }

    public static function body($data)
    {
        foreach (self::$postrules as $rule) {
            if ($rule != "" && $data != "" && preg_match("/" . $rule . "/is", urldecode($data))) {
                return true;
            }
        }
        return false;
    }

    private static function fileExtCheck($request, $response)
    {

    }
}

